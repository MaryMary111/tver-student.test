<!DOCTYPE html>
<html>
<head lang="ru">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo $seoDescription; ?>">
	<meta name="keywords" content="<?php echo $seoKeywords; ?>">
	<title><?php echo $seoTitle; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->css; ?>main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>views/css/request.view.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>views/css/service.view.css">

</head>

<body>
	<header class="section--header">
	 	
		<section class="section--top">
			<div class="top container">
				<div class="top__col-1">
					<a class="logo" href="<?php echo $mainUrl; ?>">
						<img src="<?php echo $config->urls->img;?>/logo-full.png" alt="Тверь-Студент">
					</a>
				</div>
				<div class="top__col-2">
					<p class="address-line">
						г. Тверь, ул Советская, д. 52, офис 7 (3&nbsp;этаж)
						<br>
						tver-student@mail.ru
					</p>
				</div>
				<div class="top__col-3">
					<span class="phone">
						<img src="<?php echo $config->urls->img; ?>/phone.png" class="phone__img">
						8 (904) 004-85-55
					</span>
				</div>
			</div>
	 	</section>
	 	
		<section class="section--main-menu">
		 	<nav class="menu container">
				
				<div class="menu__toggler">☰ МЕНЮ</div>
		 		<?php echo $menu; ?>
				
		 	</nav>
	 	</section>
		
	</header>
	
<?php if ($page->url == $mainUrl) { ?>
	<div class="section--ogromod">
		<div class="ogromod container">
			<h1 class="ogromod__head">
				Дипломные, курсовые,
				<br>
				контрольные работы и рефераты
			</h1>
			<p class="ogromod__under-head">
				по любым специальностям
			</p>
			
			<div class="features">
				<div class="features__item">
					<img class="features__item-img" src="<?php echo $config->urls->img; ?>/feature-clock.png">
					<span class="features__item-text">Срочные работы от 1 дня</span>
				</div>
				<div class="features__item">
					<img class="features__item-img" src="<?php echo $config->urls->img; ?>/feature-contract.png">
					<span class="features__item-text">Заключение договора</span>
				</div>
				<div class="features__item">
					<img class="features__item-img" src="<?php echo $config->urls->img; ?>/feature-exp.png">
					<span class="features__item-text">Опыт 10 лет</span>
				</div>		
			</div>
			
			<div class="order-block">
				<a class="button" href="<?php echo $orderUrl; ?>"><span>заказать</span></a>
			</div>
		</div>
	</div>
<?php } ?>

	<div class="section--page">
		<div class="container page">
			
			<div class="main content">
				<?php include ("views/{$page->template}.view.php") ?>
			</div>
			
			<div class="sidebar">

				<div class="widget--nav">	
					<div class="widget__heading">Услуги</div>
					<?php echo $menu_left; ?>
				</div>
				
				<div class="widget--reviews reviews">
				
					<div class="widget__heading">Отзывы</div>
					
					<div class="review">
						<p class="review__text">
							&laquo;Спасибо большое за работу! Уже много раз заказывала и теперь хочу зказать диплом!!! 
							Еще раз спасибо за хорошие работы! Ирина Спасибо большое.... благодаря Вам я теперь 
							дипломированная специалистка.&raquo;
						</p>
						<p class="review__author">
							Светлана Фалина
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>
	
	<div class="container section--payment">
		<div class="payment">			
			<div class="payment__block">
				<img src="<?php echo $config->urls->img; ?>/payment-purse.png" class="payment__img payment__img_purse">
			</div>
			<div class="payment__block">
				<img src="<?php echo $config->urls->img; ?>/payment-sber.png" class="payment__img payment__img_purse">
			</div>
			<div class="payment__block">
				<img src="<?php echo $config->urls->img; ?>/payment-yandex.png" class="payment__img payment__img_purse">
			</div>
			<div class="payment__block">
				<img src="<?php echo $config->urls->img; ?>/payment-card.png" class="payment__img payment__img_purse">
			</div>
			<div class="payment__block">
				<img src="<?php echo $config->urls->img; ?>/payment-wm.png" class="payment__img payment__img_purse">
			</div>
		</div>
	</div>
	
	<footer class="section--footer">
		<div class="container footer">
			<div class="footer__col-1">
				<span class="cr">&copy; 2016 tver-student.ru</span>
			</div>
			<div class="footer__col-2"> 
				<a href="" class="mail">tver-student@mail.ru</a>
			</div>
			<div class="footer__col-3">
				<span class="phone">
					<div class="phone__img"></div>
					<a href="#">8 (904) 004-85-55</a>
				</span>
			</div>
			<div class="creation">
				<a href="<?php echo $dxlabUrl ?>">Создание сайта - DXLab</a>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="<?php echo $config->urls->js; ?>/main.js"></script>
</body>
</html>