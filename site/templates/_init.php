<?php
include "partials/menu.part.php";

$seoDescription = $page->seo_description;
$seoKeywords = $page->seo_keywords;
$seoTitle = $page->get("seo_title|title");
$mainUrl = $config->urls->root;
$dxlabUrl = "http://dxlab.ru";
$orderUrl = $pages->get("id=1021")->url;

$config->urls->css = $config->urls->templates . "build/css/";
$config->urls->js = $config->urls->templates . "build/js/";
$config->urls->fonts = $config->urls->templates . "build/fonts";
$config->urls->img = $config->urls->templates . "build/img";
//echo $config->urls->css;