<h1><?php echo $page->header; ?></h1>

<div class="service__box clearfix">
        <div class="service__div-content col-1">
            <img src="<?php echo $config->urls->img;?>/service_money.png" alt="цена">
            <span class="service__col-title">от 7000 р.</span> 
        </div>
        <div class="service__div-content col-2">
             <img src="<?php echo $config->urls->img;?>/service_data.png" alt="сроки">
            <span class="service__col-title">от 10 дней</span>
        </div>
        <div class="service__div-content col-3">
             <img src="<?php echo $config->urls->img;?>/service_time.png" alt="пометка">
            <span class="service__col-title">Срочно!</span>
        </div>
</div>
<div class='service__button-div'>
    <a class='service__button' href="<?php echo $orderUrl; ?>">
        <span class='service__button-text'>ЗАКАЗАТЬ</span>
    </a>
</div>

<?php echo $page->body; ?>
