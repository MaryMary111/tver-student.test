<h1><?php echo $page->title; ?></h1>
<?php echo $page->body; ?>

<div class="contacts-box clearfix">
    <div class="contacts-box__skype">
        <img height="25px" alt="skype" src="<?php echo $config->urls->img;?>/skype.png">
        <span><?php echo $page->skype ?></span>
    </div>
    <div class="contacts-box__phone">
       <img height="25px" alt="телефон"  src="<?php echo $config->urls->img;?>/phone.png">
        <span><?php echo $page->telephone ?></span>
    </div>
</div>
<div class="contacts-address">
        <span>Наш офис находится - <?php echo $page->address ?></span>
</div>
<div class="contacts-map">
    <?php echo $page->inline_script; ?>
</div>